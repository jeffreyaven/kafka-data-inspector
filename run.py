import webapp2, logging
from paste import httpserver
from controllers import controllers
debug = True

#
# Main
#	

app = webapp2.WSGIApplication([
	(r'/topics/(\w+)', controllers.TopicHandler)
], debug=debug)

app.error_handlers[404] = controllers.handle_404
app.error_handlers[500] = controllers.handle_500

httpserver.serve(app, host='0.0.0.0', port='8080')