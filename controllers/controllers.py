import webapp2, os, sys, inspect, uuid, logging
from kafka import KafkaConsumer
from kafka.structs import TopicPartition
import globals
# from models import models

#
# Controllers
#

class TopicHandler(webapp2.RequestHandler):
    def get(self, topic_name):
		# get list of topics 
		kdi = KafkaConsumer(group_id='kafka-data-inspector',
					 bootstrap_servers=globals.bootstrap_servers)
		topics = kdi.topics() 
		if topic_name not in topics:
			self.abort(404)
		# get available partitions for topic
		topic_part_nums = kdi.partitions_for_topic(topic_name)
		# instantiate 'real' consumer
		cg = str(uuid.uuid4())
		consumer = KafkaConsumer(topic_name,
					 group_id=cg,
					 auto_offset_reset='earliest',
					 enable_auto_commit=False,
					 bootstrap_servers=globals.bootstrap_servers)		
		# get largest offset
		highest_offset = 0
		for topic_part_num in topic_part_nums:
			tp = TopicPartition(topic_name,topic_part_num)
			end_offset = consumer.end_offsets([tp])[tp]-1
			if end_offset > highest_offset:
				highest_offset = end_offset
		# get messages
		messages = []
		for message in consumer:
			msg = {}
			if message.offset >= end_offset:
				consumer.close()
				consumer.unsubscribe()		
				break
			else:
				msg["partition"] = message.partition
				msg["offset"] = message.offset
				msg["key"] = message.key
				msg["value"] = message.value
				messages.append(msg)
		template_values = {
					'topic_name' : topic_name,
					'messages': messages,
				}
		template = globals.JINJA_ENVIRONMENT.get_template('index.html')
		self.response.write(template.render(template_values))
		
#
# Error Handlers
#		

def handle_404(request, response, exception):
	logging.exception(exception)
	template_values = {
				'message': 'Oops! I could swear this page was here!',
			}
	template = globals.JINJA_ENVIRONMENT.get_template('error.html')
	response.write(template.render(template_values))	
	response.set_status(404)	

def handle_500(request, response, exception):
	logging.exception(exception)
	template_values = {
				'message': 'A server error occurred!',
			}
	template = globals.JINJA_ENVIRONMENT.get_template('error.html')
	response.write(template.render(template_values))	
	response.set_status(500)