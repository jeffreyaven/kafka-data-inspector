import os, jinja2

#
# Kafka
#
bootstrap_servers=['localhost:9092']

#
# Templating Vars
#

template_dir = os.path.join(os.path.dirname(__file__), 'views')
JINJA_ENVIRONMENT = jinja2.Environment(
					loader=jinja2.FileSystemLoader(template_dir),
					extensions=['jinja2.ext.autoescape','jinja2.ext.loopcontrols'],
					autoescape=True)